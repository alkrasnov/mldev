# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

image: ubuntu:18.04

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  paths:
    - .cache/pip
    - venv/
    
stages:
  - build
  - test
  - deploy
  - templates

before_script:
  - apt-get update
  - apt-get install -y git curl python3-pip python3-dev python3-venv
  - echo "Setting up venv"
  - python3 -m venv venv
  - source ./venv/bin/activate
  - python3 -m pip install --upgrade pip setuptools wheel
#  - pip --no-cache-dir install -e .
#  - pip freeze

build:
  stage: build
  script:
    - echo "Running build..."
    - pip install -e .[base,jupyter,collab]
    - pip freeze
  only:
    - merge_requests

test:
  stage: test
  script:
    - echo "Running test..."
    - pip install -e .[base,jupyter,collab]
    - pip install -r ./test/requirements.txt
    - py.test -v
    - pip install flake8  # you can also use tox
    # do not fail build even if errors, for now
    - flake8 --exit-zero ./src
    # - tox -e py36, flake8 - todo test installed version in a venv
  only:
    - merge_requests

dist:
  stage: deploy
  script:
    - python setup.py clean build bdist_wheel sdist
    - |
      set -x
      for extra in bot base dvc tensorboard controller jupyter collab
      do
        whl=(dist/*.whl)
        targz=(dist/*.tar.gz)
        pip install mldev[$extra]@file://$(pwd)/${whl}
        pip uninstall -y mldev
        pip install mldev[$extra]@file://$(pwd)/${targz}
        pip uninstall -y mldev
      done
  artifacts:
    paths:
      - dist/*.whl
      - dist/*.tar.gz
  only:
    - merge_requests
    - develop

develop:
  stage: deploy
  script:
    - curl --silent https://gitlab.com/mlrep/mldev/-/raw/develop/install_mldev.sh -o install_mldev.sh && chmod +x ./install_mldev.sh
    - yes | ./install_mldev.sh bot base jupyter dvc tensorboard controller collab && mldev version
    - pip install -r ./test/requirements.txt
    - py.test -v
  only:
    - develop

pages:
  stage: deploy
  script:
    - |
      apt-get update && apt-get install -y language-pack-ru
      echo LANG=ru_RU.UTF-8 > /etc/default/locale
    - pip install -r ./docs/requirements.txt
    - /bin/bash -c "export LC_CTYPE="ru_RU.UTF8"; cd docs ; make clean wiki html"
    - mv docs/build/html public
  artifacts:
    paths:
      - public
  rules:
    - allow_failure: true
      when: manual

bridge_default:
  stage: templates
  trigger:
    project: mlrep/template-default
    branch: master
  only:
    - develop

bridge_full:
  stage: templates
  trigger:
    project: mlrep/template-full
    branch: master
  only:
    - develop
# this is the fix numpy 1.20+ and scipy 1.6+ needs python 3.7
scipy~=1.5.4; python_version == '3.6'
numpy~=1.19.5 ; python_version == '3.6'
protobuf~=3.14.0; python_version == '3.6'
pillow~=8.1.0; python_version == '3.6'
six >=1.13.0, ==1.*; python_version == '3.6'
matplotlib~=3.3.4; python_version == '3.6'
# end of fix
scipy
numpy
protobuf
pillow
six
ipython>=7.16.1
nbformat>=5.1.3
markdown-it-py>=1.1.0
dill>=0.3.3
matplotlib
